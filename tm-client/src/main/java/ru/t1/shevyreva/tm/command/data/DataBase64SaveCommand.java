package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.data.DataSaveBase64Request;
import ru.t1.shevyreva.tm.enumerated.Role;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save Data to base64 file.";

    @NotNull
    private final String NAME = "data-save-base64";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final DataSaveBase64Request request = new DataSaveBase64Request(getToken());
        getDomainEndpoint().saveDataBase64(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
