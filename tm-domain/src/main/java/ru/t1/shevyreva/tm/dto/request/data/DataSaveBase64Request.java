package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataSaveBase64Request extends AbstractUserRequest {

    public DataSaveBase64Request(@Nullable String token) {
        super(token);
    }

}
