package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ISessionRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IDatabaseProperty;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull IDatabaseProperty databaseProperty;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;


    public ConnectionService(@NotNull IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getConnection(){
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = databaseProperty.getDatabaseUsername();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseUrl();
        @NotNull final String driver = databaseProperty.getDatabaseDriver();

        final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);

        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
