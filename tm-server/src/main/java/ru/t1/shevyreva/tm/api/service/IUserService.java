package ru.t1.shevyreva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    @SneakyThrows
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    @SneakyThrows
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    @SneakyThrows
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    @SneakyThrows
    User findByLogin(@Nullable String login);

    @NotNull
    @SneakyThrows
    User findOneById(@Nullable final String id);

    @NotNull
    @SneakyThrows
    User findByEmail(@Nullable String email);

    @NotNull
    @SneakyThrows
    User removeByLogin(@Nullable String login);

    @NotNull
    @SneakyThrows
    User removeByEmail(@Nullable String email);

    @NotNull
    @SneakyThrows
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    @NotNull
    @SneakyThrows
    User setPassword(@Nullable String id, @Nullable String password);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull
    @SneakyThrows
    User lockUser(@NotNull final String login);

    @NotNull
    @SneakyThrows
    User unlockUser(@NotNull final String login);

    @NotNull
    @SneakyThrows
    Collection<User> set(@NotNull final Collection<User> models);

    @SneakyThrows
    void removeAll();

    @NotNull
    @SneakyThrows
    Collection<User> add(@NotNull final Collection<User> models);

    @SneakyThrows
    List<User> findAll();

    @NotNull
    @SneakyThrows
    User removeOneById(@Nullable final String id);

}
