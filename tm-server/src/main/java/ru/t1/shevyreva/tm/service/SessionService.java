package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.ISessionRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.ISessionService;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.model.Session;


import java.sql.Connection;
import java.util.Collection;
import java.util.List;

public class SessionService implements ISessionService {

    private final IConnectionService connectionService;

    public SessionService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    public Session add(
            @Nullable final String userId,
            @Nullable final Session session
    ){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        session.setUserId(userId);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try{
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            sqlSession.commit();
            return session;
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Session add(
            @Nullable final Session session
    ){
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try{
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            add(session.getUserId(), session);
            sqlSession.commit();
            return session;
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Session findOneById(@Nullable final String userId, @Nullable final String id){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        try(@NotNull final SqlSession sqlSession = connectionService.getConnection()){
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final Session session = findOneById(userId, id);
            if (session == null) throw new TaskNotFoundException();
            return session;
        }
    }

    @NotNull
    @SneakyThrows
    public Session removeOne(@Nullable final Session session){
        if (session == null) throw new ModelNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try{
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeOne(session);
            sqlSession.commit();
            return session;
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Session removeOneById(@Nullable final String userId, @Nullable final String id){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try{
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            Session session = findOneById(userId, id);
            sessionRepository.removeOneById(userId, id);
            sqlSession.commit();
            return session;
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId){
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try{
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clear(userId);
            sqlSession.commit();
        }
        catch(@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public List<Session> findAll(){
        try(@NotNull final SqlSession sqlSession = connectionService.getConnection()){
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<Session> add(@NotNull final Collection<Session> models) {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            for (@NotNull Session session : models) {
                sessionRepository.add(session);
            }
            sqlSession.commit();
            return models;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll(){
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try{
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<Session> set(@NotNull final Collection<Session> models) {
        @Nullable final Collection<Session> entities;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            removeAll();
            entities = add(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return entities;
    }

    public boolean existsById(@NotNull final String id){
        try(@NotNull final SqlSession sqlSession = connectionService.getConnection();){
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return (sessionRepository.findOneById(id) != null);
        }
    }

}
