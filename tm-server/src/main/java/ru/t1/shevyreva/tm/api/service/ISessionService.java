package ru.t1.shevyreva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Session;

public interface ISessionService{

    @NotNull
    @SneakyThrows
    Session add(@Nullable final String userId, @Nullable final Session session);

    @NotNull
    @SneakyThrows
    Session add(@Nullable final Session session);

    @NotNull
    @SneakyThrows
    Session removeOne(@Nullable final Session session);

    @SneakyThrows
    boolean existsById(@NotNull final String id);

}
