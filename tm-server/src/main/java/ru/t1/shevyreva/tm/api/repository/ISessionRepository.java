package ru.t1.shevyreva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Session;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ISessionRepository{

    @Insert("INSERT INTO tm_session (id, role, created, user_id) " +
            "VALUES (#{id},#{role},#{date}, #{userId});")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    void add(@NotNull Session session);

    @Insert("INSERT INTO tm_session (id, user_id, role, created " +
            "VALUES (#{id},#{userId},#{role},#{date}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull Session session);

    @Update("UPDATE tm_session SET user_id = #{userId}," +
            " role =#{role} " +
            "WHERE id = #{id}")
    void update(@NotNull final Session session);

    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    @Nullable
    List<Session> findAll();

    @Delete("TRUNCATE TABLE tm_session;")
    void removeAll();

    @Delete("DELETE FROM tm_session WHERE id = #{id};")
    void removeOne(@NotNull final Session session);

    @Delete("DELETE FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    void removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    @Nullable
    Task findOneById(@NotNull @Param("id") String id);

}
